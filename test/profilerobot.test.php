<?php 
require_once("../components/imagehandling/image.php");
class TestStack {
	// tests the construction and getters of image.php
	public function testImage() {
		// for reference, se profilerobot.model.php
		try {
			$socialmediaArray = array(
			"company" => "teztCompany",
			"dimensions" => array(10, 10)
			);
			$teztImg = new \profilerobot\image\Image($socialmediaArray["company"], $socialmediaArray["dimensions"]);
			echo "<br><br>
			<strong>testImage() passed</strong>
			<br><br>";
		}
		catch (\Error $e) {
			throw new \Error("Image::__construct broken.");
		}
		if (!$teztImg->getDimensions()) {
			throw new \Error("Image::getDimensions() broken.");	
		}
	} 
}

$stacktest = new TestStack();
$stacktest->testImage();