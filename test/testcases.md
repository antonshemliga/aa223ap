#Testcases for Profile Robot

> All these should work. Or you should make them work.

- [x] TC 0: Stresstest by Gremlins.js
```
__INPUT__:
* Run Gremlins.js

__OUTPUT__:
* Nothing severe happens


- [x] __TC 1.1: User visits page.__

```
__INPUT__:
* User visits page

__OUTPUT__:
* Page renders upload form, instructions and welcome message.
```

- [x] __ TC 1.2: User clicks instructions.__
```
__INPUT__:
* TC 1.1
* User clicks `instructions`.

__OUTPUT__:
* Page renders modal with instructions.

- [x] __ TC 1.3: User closes instructions.__
```
__INPUT__:
* TC 1.1
* TC 1.2

__OUTPUT__:
* Page displays upload form, instructions and welcome message.

```
- [x] __ TC 1.4: User clicks browse. 
```
__INPUT__:
* TC 1.1
* User clicks `let me browse a selfie`

__OUTPUT__:
* File upload window is displayed

```
- [x] __ TC 1.5: User uploads to large image. 
```
__INPUT__:
* TC 1.4
* User uploads non-corrupt file with filesize larger than 2 mb

__OUTPUT__:
* Error message "File uploaded is too large. Please upload an image with file size less than 2 mb.".
* User can upload once more.

```

```
- [x] __ TC 1.6: User uploads to file that isn't jpeg or png. 
```
__INPUT__:
* TC 1.4
* User uploads non-corrupt file with other file extension than png or jpeg

__OUTPUT__:
* Error message "The image is broken :( Are you sure you uploaded a jpeg or png? Sorry for the inconvenience."
* User can upload once more.

```

```
- [x] __ TC 1.7: Users connection is broken to server when uploading. 
```
__INPUT__:
* TC 1.4
* User uploads non-corrupt file with filesize less than 2 mb and file extension jpeg or png

__OUTPUT__:
* Error message "Oh no, the server is upset. Sorry for the inconvenience, your images couldn't be generated. Please contact me at jag@antonkandersson.se if this doesn't work next time you try."
* User can upload once more.

```
- [x] __ TC 1.5: User uploads correct png. __
```
__INPUT__:
* TC 1.1
* TC 1.4
* User chooses a non-corrupt png with a filesize lesser than 2 mb
* User clicks "upload image"

__OUTPUT__:
* Image is uploaded to server
* New view is generated, which displays the image, links to zipfile, all the social media currently supported by Profile Robot, see Readme.md. Filename and permalink is also present.
```

- [x] __ TC 1.6: User uploads correct png. __
```
__INPUT__:
* TC 1.1
* TC 1.4
* User chooses a non-corrupt jpeg with a filesize lesser than 2 mb
* User clicks "upload image"

__OUTPUT__:
* Image is uploaded to server
* New view is generated, which displays the image, links to zipfile, all the social media currently supported by Profile Robot, see Readme.md. Filename and permalink is also present.
```

Also list:
what happens when each social media link is clicked,
when user shares link
the prefix for the files
the content of the zipfiles
the generated view
the url in browser
