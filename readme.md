#Profile Robot

## About

Profile Robot makes creating profile pictures for all of your social media easy. 

The app takes one original image, processes it and then generates profile pictures. The images can then be downloaded in a zipfile or as separate profile pictures.

## Demo

Live demo @ [antonkandersson.se/profilerobot](http://www.antonkandersson.se/profilerobot)

## Supported social media

* [Facebook](https://facebook.com)

* [Google+](https://plus.google.com)  
(by changing Google+, you update all of Google's services)

* [Twitter](https://twitter.com)

* [LinkedIn](https://linkedin.com)

* [Pinterest](https://pinterest.com)

* [Instagram](https://instagram.com)

Suggestions of more social media to support are welcomed as issues.

## Compability

Profile Robot was created with the intent to be easy to install. It's written in PHP5.3 since most cheap webhosts support it.

## Installation

FTP into your webhost, which supports PHP 5.3, and upload this repo. Your server should have writing permission.

You might have to create an empty folder named `uploads` in the same folder you have `app`. This folder is .gitignored in development and therefore won't show up in your filetree if you just clone this repo.

Your folder structure should look like:

```
/app
|
|___ profilerobot.model.php
|___ profilerobot.view.php
|___ profilerobot.controller.php
|___ /image
|_________ image.model.php
|_________ image.view.php
|_________ image.controller.php
|___ /html
|_________ html.templates.php
|___ /assets
|_________ /css
|__________________ material.min.css
|__________________ material-wfont.min.css
|__________________ ripples.min.css
|_________ /js
|__________________ material.min.js
|__________________ ripples.min.js
|_________ /img
|__________________ example.jpg
|_________ /fonts
|__________________ LICENSE.txt
|__________________ Material-Design.eot
|__________________ Material-Design.svg
|__________________ Material-Design.woff
|__________________ Material-Design.ttf

/uploads (okay with empty folder, but not to leave it out)
index.php
```

It's probably full of favicons too. :)

## Exceptions

Exceptions are silent, and logged in `errorlog.txt`. Program tries to restart and gives user error message instead.

