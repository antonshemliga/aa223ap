<?php
	namespace profilerobot;
	require_once("./app/profilerobot.controller.php");
	$profileRobotController = new controller\ProfileRobotController();
	$profileRobotController->handleRequest();
	$profileRobotController->renderPage();