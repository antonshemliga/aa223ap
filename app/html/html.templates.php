<?php 
	namespace	profilerobot\view;
	require_once("./app/logger/error.logger.php");
	require_once("./app/exceptions/echohtml.exception.php");
	
	class HTMLTemplate {
		private $logger;

		public function __construct () {
			$this->logger = new \errors\logger\Logger();
		}

		public function element ($element, $text) {
			if (!$element || !$text || !is_string($text) || !is_string($element)) {
				$this->logger->logError(new \profilerobot\errors\EchoHTMLException());
			}
			return '<'.$element.'>'.$text.'</'.$element.'>';
		}

		public function closingTag ($element) {
			if (!$element || !is_string($element)) {
				$this->logger->logError(new \profilerobot\errors\EchoHTMLException());
			}
			return '</'.$element.'>';
		}

		public function warning ($text) {
			return 
			'<div class="alert alert-dismissable alert-warning">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<h4>Warning!</h4>
				<p>'.$text.'</p>
			</div>';
		}

		public function img ($src, $width = null, $height = null) {
			if ($width == null || $height == null) {
				return '<img src="'.$src.'" style="max-width: 90%; height: auto;">';
			}
			return '<img src="'.$src.'" width="'.$width.'" height="'.$height.'" style="max-width: 90%; height: auto;">';
		}
		public function success ($uploadedFileName) {
			return 
			'<div class="alert alert-success">
				<h2><strong>'.$uploadedFileName.'</strong> in a whole bunch of sizes.</h2>
				<a href="uploads/facebook-'.$uploadedFileName.'"><i class="icon icon-material-post-facebook"></i></a>
				<a href="uploads/twitter-'.$uploadedFileName.'"><i class="icon icon-material-post-twitter"></i></a>
				<a href="uploads/linkedin-'.$uploadedFileName.'"><i class="icon icon-material-post-linkedin"></i></a>
				<a href="uploads/googleplus-'.$uploadedFileName.'"><i class="icon icon-material-post-gplus"></i></a>
				<a href="uploads/instagram-'.$uploadedFileName.'"><i class="icon icon-material-post-instagram"></i></a>
				<a href="uploads/pinterest-'.$uploadedFileName.'"><i class="icon icon-material-post-pinterest"></i></a>
				<a href="uploads/all-'.$uploadedFileName.'.zip"><i class="icon icon-material-file-download"></i></a>
			</div>';	
		}

		public function header () {
			return '<!doctype html>
			<html lang="sv">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png">
			<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png">
			<link rel="icon" type="image/png" href="favicon-192x192.png" sizes="192x192">
			<link rel="icon" type="image/png" href="favicon-160x160.png" sizes="160x160">
			<link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
			<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
			<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
			<meta name="msapplication-TileColor" content="#da532c">
			<meta name="msapplication-TileImage" content="mstile-144x144.png">
			<title>Profile Robot</title>
			<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
			<link href="app/assets/css/material-wfont.min.css" rel="stylesheet" type="text/css">
			<link href="app/assets/css/ripples.min.css" rel="stylesheet" type="text/css">
			</head>
			<body class="container-fluid pull-left">';
		}

			public function generatedHeader () {
			return '<!doctype html>
			<html lang="sv">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png">
			<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png">
			<link rel="icon" type="image/png" href="favicon-192x192.png" sizes="192x192">
			<link rel="icon" type="image/png" href="favicon-160x160.png" sizes="160x160">
			<link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
			<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
			<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
			<meta name="msapplication-TileColor" content="#da532c">
			<meta name="msapplication-TileImage" content="mstile-144x144.png">
			<title>Profile Robot: Oh, look at you!</title>
			<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
			<link href="../../app/assets/css/material-wfont.min.css" rel="stylesheet" type="text/css">
			<link href="../../app/assets/css/ripples.min.css" rel="stylesheet" type="text/css">
			</head>';
		}
	
	public function scripts () {
		return '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="app/assets/js/ripples.min.js"></script> 
		<script src="app/assets/js/material.min.js"></script>';
	}

	public function generatedScripts () {
		return '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="../../app/assets/js/ripples.min.js"></script> 
		<script src="../../app/assets/js/material.min.js"></script>';
	}

	public function container () {
		return '<body class="container-fluid" style="margin-top: 20px;">';
	}
	public function form($filename) {
		$file = $filename ? $filename : 'file';
		return '<form class="form-horizontal" method="post" enctype="multipart/form-data">
				    <fieldset>
			        <legend>Choose picture</legend>
			        <div class="form-group">
			            <div class="col-lg-10">
                    <input type="text" readonly="" class="form-control floating-label" placeholder="Let me browse a selfie...">
		               	<input type="file" name=' . $file . ' id="inputFile" multiple="" autofocus>
						        <small>Smaller than 2mb and jpg/jpeg/png, please.</small>
				          </div>
				        </div>
				        <div class="form-group">
				          <div class="col-lg-12">
				            <button type="submit" class="btn btn-primary"><i class="icon-material-file-upload"></i> Upload photo</button>
				            <button type="button" data-toggle="modal" data-target="#instructions" class="btn btn-default"><i class="icon-material-help"></i> Instructions</button>
				            <div class="modal" id="instructions">
									    <div class="modal-dialog">
									      <div class="modal-content">
									        <div class="modal-header">
									          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									            <h3 class="modal-title">Create all your profile pictures from one original.</h3>
									    	      </div>
									            <div class="modal-body">
									            	<h4>How this works</h4>
									            	<p>
									            		First, you need to take a picture. Done? Okay, let\'s create all your Mini-Mes.
									            	</p>
									            	<ol>
									            		<li>Click the field urging you to browse for a selfie.</li>
									            		<li>Now, choose your favorite picture of yourself.<br>
									            			<small>Image should be cropped, less than 2 mb in size and a png or jpeg.</small>
									            		</li>
									            		<li>Click upload.</li>
									            		<li>Share your link with all your friends.</li>
									            	</ol>
									            	
									            	<h4>Supported social media</h4>
									            	<p>
									            		In no particular order:
									            	</p>
									            	<ul>
									            		<li>Facebook</li>
									            		<li>Google+</li>
									            		<li>LinkedIn</li>
									            		<li>Twitter</li>
									            		<li>Instagram</li>
									            		<li>Pinterest</li>
									            	</ul>

									            	<h4>Examples</h4>
									            	<p>Do this:</p>
									            	<p>
										            	<img src="app/assets/img/example.jpg" alt="omg such a cute kid!"><br>
										            	<small><strong>example.jpg</strong> ~300kb, 960x1280 pixels.</small>
									            	</p>
									            	<ul>
									            	<li>Upload an image that\'s below 2mb, with your face (and maybe a cute baby) in it.</li>
									            	</ul>
									            	<p>Don\'t do this:</p>
									            	<ul>
									            		<li>Upload an image that\'s crazy large</li>
									            		<li>Upload an image that\'s uncropped - Profile Robot will only resize your image</li>
									            		<li>Upload something that\'s not a profile picture</li>
									             	</ul>
									            </div>
									            <div class="modal-footer">
									      	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									            </div>
									        </div>
									    </div>
										</div>
				          </div>
				        </div>
				    </fieldset>
				</form>';
		}

		public function row () {	
			return '<div class="row">';
		}

		public function column () {
			return '<div class="col-md-4">';
		}
		public function sidebar ($filename, $imgsrc, $prefix) {
			if (!$filename || !is_string($filename) || !$imgsrc || !is_string($imgsrc)) {
				$this->warning("Oops. Something went, terribly, terribly wrong.");
				return;
			}
			$explosion = explode("/", $imgsrc);
			$extplosion = explode(".", $explosion[2]); //omg that's so funny
			$name = $extplosion[0];
			$pre = $prefix ? $prefix : null;
			$zip = ".zip";

			
			return '<div class="alert alert-success" style="overflow: hidden;">
								<h1>Here you go!</h1>
								<h3><strong>'.$filename.'</strong> in a whole bunch of sizes.</h3>
								<p>Click and save each image or <a href="'.$pre.$name.$zip.'" download>download as a zip</a></p>
							</div>
							<a href="../../index.php" class="btn btn-default"><i class="icon-material-arrow-back"></i> Back to the robot</a>';
		}

		public function generatedImages ($imgsrc, $prefix) {
			if (!$imgsrc || !is_string($imgsrc)) {
				$this->warning("Oops. Something went, terribly, terribly wrong.");
				return;
			}
			$explosion = explode("/", $imgsrc);
			$filename = $explosion[2];
			$extplosion = explode(".", $filename); //omg that's so funny
			$name = $extplosion[0];
			// easier to spot and change filenames in return when they're in vars
			$facebook = "facebook-";
			$twitter = "twitter-";
			$linkedin = "linkedin-";
			$googleplus = "googleplus-";
			$reddit = "reddit-";
			$instagram = "instagram-";
			$pinterest = "pinterest-";
			$pre = $pre = $prefix ? $prefix : null;
			$zip = ".zip";
			$url = "http://www.antonkandersson.se/profilerobot/".$explosion[0]."/".$explosion[1]."/";
			return 
			'<div class="col-md-8">
				<div class="form-group has-success col-md-4">
				<img src="'.$facebook.$filename.'" alt="Your beautiful self" style="max-width: 450px; height: auto; margin: 0 auto;">
				<form class="form-horizontal">
					<label for="share-link"><i class="icon-material-share"></i> Share this link:</label>
						<input value="'.$url.'" id="share-link" class="form-control" style="width: 450px;">
					</form>
				</div>
				<div class="col-md-4 pull-right">
					<p style="font-size: 30px;">
						<a href="'.$pre.$name.$zip.'" download><i class="icon icon-material-file-download"></i>
						 Zipfile
						</a>
					</p>
					<p style="font-size: 30px;">
						<a href="'.$facebook.$filename.'" download><i class="icon icon-material-post-facebook"></i>
						 Facebook
						</a>
					</p>
					<p style="font-size: 30px;">
						<a href="'.$googleplus.$filename.'" download><i class="icon icon-material-post-gplus"></i>
							Google+
						</a>
					</p>
					<p style="font-size: 30px;">
						<a href="'.$twitter.$filename.'" download><i class="icon icon-material-post-twitter"></i>
							 Twitter
							</a>
						</p>
						<p style="font-size: 30px;">
							<a href="'.$pinterest.$filename.'" download><i class="icon icon-material-post-pinterest"></i>
							 Pinterest
							</a>
						</p>
						<p style="font-size: 30px;">
							<a href="'.$instagram.$filename.'" download><i class="icon icon-material-post-instagram"></i>
							 Instagram
							</a>
						</p>
						<p style="font-size: 30px;">
							<a href="'.$linkedin.$filename.'" download><i class="icon icon-material-post-linkedin"></i>
							 LinkedIn
							</a>
						</p>
				</div>';
		}
	}
