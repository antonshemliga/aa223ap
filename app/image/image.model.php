<?php
	namespace image\model;
	require_once("./app/exceptions/fileerror.exception.php");
	require_once("./app/exceptions/filemove.exception.php");
	require_once("./app/logger/error.logger.php");
	class ImageModel {

		// string dependencies
		private $sharedStrings;
		private $errorMessages = array(
			"fileSize" => "File uploaded is too large. Please upload an image with file size less than 2 mb.",
			"buffer" => "Something went wrong, your page couldn't be generated.",
			"fileError" => "The image is broken :( Are you sure you uploaded a jpeg or png? Sorry for the inconvenience.",
			"generic" => "Oh no, the server is upset. Sorry for the inconvenience, your images couldn't be generated. <br> Please contact me at jag@antonkandersson.se if this doesn't work next time you try.",
		);
		private $identity;
		private $logger;
		public function __construct ($id) {
			$this->setIdentity($id);
			$this->createSharedArray();
			$this->logger = new \errors\logger\Logger();
		}
		
		// populateArray should contain all shared string deps
		// when run, it sets the sharedStrings - only update initial shared strings here.
		private function populateArray () {
			$sharedStringDependencies = array();
			$sharedStringDependencies["fileAssociation"] = $this->identity."::Upload";
			$sharedStringDependencies["prefix"] = "antonkandersson.se-";
			$sharedStringDependencies["identity"] = $this->identity;
			return $sharedStringDependencies;
		}

		public function createSharedArray () {
			$this->sharedStrings = $this->populateArray();
		}

		/*** getters ***/
		// returns this app's prefix
		public function getPrefix () {
			return $this->sharedStrings["prefix"];
		}

		public function getFileAssociation () {
			return $this->sharedStrings["fileAssociation"];
		}

		public function getIdentity () {
			return $this->identity;
		}

		public function getError () {
			return (isset($_SESSION[$this->identity]) ? $_SESSION[$this->identity] : false);
		}

		/*** setters ***/
		public function setIdentity ($id) {
			if (!$id || strlen($id) === 0) {
				$this->logger->logError(new \profilerobot\errors\FileErrorException());
			}
			$this->identity = $id;
		}

		public function setSessionError ($errormsg) {
			$_SESSION[$this->identity] = $errormsg;
		}

		/*** validation ***/
		// true if error is found
		// this one's logic is upside down from the others - if true, error has happened
		// therefore, if false, no error has happened and the upload is ok
		// and if true, the generic error message is set
		public function error ($upload) {
			return (isset($upload["error"]) && $upload["error"] > 0)
						 ? $this->setSessionError($this->errorMessages["generic"])
						 : false;
		}
		
		
		// true if img is smaller than 2mb
		// if false, sets appropriate error message about filesize in session
		public function checkSize ($upload) {
			return (isset($upload["size"])
						 && $upload["size"] < 2000*1024)
						 ? true
						 : $this->setSessionError($this->errorMessages["fileSize"]);
		}
		
		// true if file type is png or jpg
		// if false, sets appropriate error message about filetype in session
		public function checkType ($upload) {
			return (isset($upload["type"]) 
						 && $upload["type"] === "image/png" ||
						 		$upload["type"] === "image/jpg" || 
						 		$upload["type"] === "image/jpeg") 
								? true 
								: $this->setSessionError($this->errorMessages["fileError"]);
		}
		/*** manipulation ***/
		
		// creates the source file
		// only png or jpegs
		public function create ($image, $extension) { 
			$img;
			if ($extension === "png") {
				$img = @imagecreatefrompng($image);
			}

			if ($extension === "jpeg" || $extension === "jpg") {
				$img = @imagecreatefromjpeg($image);
			}
			return $img;
		}

		// saves the source file to disk
		public function save ($image, $filename, $extension) { 
			$img;
			if ($extension === "png") {
				imagepng($image, $filename);
			}

			if ($extension === "jpeg" || $extension === "jpg") {
				imagejpeg($image, $filename);
			}			
		}

		// copies the file from original image, reformats it
		private function copy ($original, $imagename, $width, $height, $extension, $path) {
			if (
					!$original  || !is_string($original)  ||
					!$extension || !is_string($extension) ||
					!$imagename || !is_string($imagename) || 
					!$width     || !is_numeric($width)    ||
					!$height    || !is_numeric($height) 
				) {
				$this->logger->logError(new \profilerobot\errors\FileErrorException());
			}
			// creates filenames that looks like this: twitter-filename.jpg
			$explosion = explode("/", $original);
			$filename = "-".$explosion[2];
			$img = $this->create($original, $extension);
			$img_name = strtolower($imagename).$filename;
			$img_x = imagesx($img);
			$img_y = imagesy($img);
			
			// proportions are based on largest px value of original image
			//
			// percentage of original width times original height
			$proportionalHeight = ($width / $img_x) * $img_y; 

			// percentage of original height times original width
			$proportionalWidth = ($height / $img_y) * $img_x;
			
			// values are set based on largest px value from w or h
			$copy_y = ($img_y > $img_x || $img_y === $img_x) ? $height : $proportionalHeight;
			$copy_x = ($img_x > $img_y || $img_x === $img_y) ? $width : $proportionalWidth;

			// creates copy, resamples it.
			$copy = imagecreatetruecolor($copy_x, $copy_y);
			imagecopyresampled($copy, $img, 0, 0, 0, 0, $copy_x, $copy_y, $img_x, $img_y);
			$this->save($copy, ($path."/".$img_name), $extension);
			
			return array(
				"fullpath" => ($path."/".$img_name),
				"filename" => $img_name,
				"width" => $copy_x,
				"height" => $copy_y
			);
		}

		// creates a dir in root/uploads in the form of a tempstring
		// example: "uploads/sharelink_32ffaSd"
		public function addDirectory ($path) {
			$explosion = explode("/", $path);
			$folder = $explosion[0]."/".$explosion[1];
			// does path exist? create a dir. 
			if ($path) {
				try {
					// /uploads in root folder for ProfileRobot
					mkdir($folder);
				}
				catch (\profilerobot\errors\FileMoveException $e) {
					$this->logger->logError(new $e);
					return false;
				}
			}
		}

		// moves uploaded file into uploaded folder, with tmp_name as prefix
		public function saveOriginal ($tempfile, $newFile) {
			if ($tempfile && $newFile) {
				try { 
					return move_uploaded_file($tempfile, $newFile);
				}
				catch (\profilerobot\errors\FileMoveException $e) {
					$this->logger->logError($e);
					return false;
				}
			}
		}

		public function createCopies ($originalReference, $extension, array $copiesToCreate, $path) {
			if (
				  !$originalReference ||
				  !$extension         ||
				  !$copiesToCreate    ||
				  !$path
				  ) {
				$this->logger->logError(new \profilerobot\errors\FileErrorException());
			}

			// $copies will contain copies in following format:
			// array(
			// 	"fullpath" => "path",
			// 	"filename" => "$img_name",
			// 	"width" => "123",
			// 	"height" => "123"
			// 	);
			$copies = array();
			for ($i = 0; $i < count($copiesToCreate); $i+=1) {
				$copies[$i] = $this->copy($originalReference, $copiesToCreate[$i]["name"], $copiesToCreate[$i]["dimensions"][0], $copiesToCreate[$i]["dimensions"][1], $extension, $path);
			}
			return $copies;
		}
		// writes errors to disk
		public function logError ($errormsg) {
			$this->logger->logError($errormsg);
		}
	}