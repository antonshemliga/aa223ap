<?php 
	namespace image\controller;
	require_once("image.model.php");
	require_once("image.view.php");
	require_once("./app/exceptions/zip.exception.php");	
	class ImageController {
		private $model;
		private $view;
		public function __construct ($identity = null) {
			if ($identity == null) {
				$identity = "ImageHandling";
			}
			$this->init($identity);
		}

		private function init ($id) {
			$this->model = new \image\model\ImageModel($id);
			$this->view = new \image\view\ImageView($this->model);
		}

		// facades for usage from other controllers
		
		// returns true if a user uploads a file that matches the identity of `$identity::Upload`
		// (see model)
		public function fileIsUploaded () {
			return $this->view ? $this->view->pictureIsUploaded() : null;
		}

		public function save () {
			$this->view->saveImage();
			$this->model->addDirectory($this->view->getImgSrc());
			$this->model->saveOriginal($this->view->getTempfile(), $this->view->getImgSrc());
		}

		public function validate () {
			return $this->view ? $this->view->validate() : null;
		}

		public function file () {
			return $this->view ? $this->view->file($this->model->getFileAssociation()) : null;
		}

		public function fileAssociation () {
			return $this->model ? $this->model->getFileAssociation() : null;
		}

		public function error () {
			return $this->model ? $this->model->getError() : null;
		}

		public function name () {
			return $this->view ? $this->view->getName() : null;
		}

		public function imgsrc () {
			return $this->view ? $this->view->path()."/".$this->view->getName() : null;	
		}

		public function reroute () {
			return $this->view->reroute();
		}

		public function displayError () {
			return $this->model ? $this->model->getError() : null;
		}

		// $copylist should be formatted as:
		// array("name" => "Facebook", "dimensions" => array(336, 336))
		// returns a nested array of copies, with each element formatted as
		// array(
		// 	"fullpath" => "path",
		// 	"filename" => "$img_name",
		// 	"width" => "123",
		// 	"height" => "123"
		// 	);
		public function copy (array $copylist) {
			return $this->model->createCopies($this->imgsrc(), $this->view->getFileExtension(), $copylist, $this->view->path());
		}
		
		// generates a static page from buffered view
		public function staticPage ($static) {
   		$dir = $this->view->path();
   		$file = $dir . "/index.html";
	    @chmod($file, 0755);
   		$write = fopen($file, "w");
   		fputs($write, $static, strlen($static));
			fclose($write);
		}

		// zips all files from current $this->view->path() into filename.zip
		public function zip () {
			$explosion = explode(".", $this->name());
			// $zipfile example: uploads/php3232dfklds/filename.zip
			$zipfile = $this->view->path()."/".$this->model->getPrefix().$explosion[0].".zip";
			$archive = new \ZipArchive();
			if (!$archive->open($zipfile, \ZIPARCHIVE::OVERWRITE)) {
			  $this->model->logError(new \profilerobot\errors\ZipException());
			}
			// $explosion[1] is the fileextension for the imagefile
			$archive->addGlob($this->view->path()."/*.".$explosion[1]);
			
			if (!$archive->status == \ZIPARCHIVE::ER_OK) {
			 $this->model->logError(new \profilerobot\errors\ZipException());
			}
			$archive->close();
		}

		public function prefix () {
			return $this->model ? $this->model->getPrefix() : null;
		} 
	}