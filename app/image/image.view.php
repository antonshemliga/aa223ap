<?php 
	namespace image\view;
	require_once("./app/exceptions/filemove.exception.php");
	require_once("./app/exceptions/fileerror.exception.php");
	class ImageView {
		// shared model with controller
		private $model;
		// container for shared view references
		private $references = array();
		// gives user feedback if an error occurs
		private $error;				
		//should only be run on construction
		private function init ($model) {
			$this->model = $model;
		}

		public function __construct (\image\model\ImageModel $imageModel) {
			$this->init($imageModel);
		}
			
		private function addReferences () {
			$tempReferences = array();
			$tempReferences["fileExtension"] = "";
			$tempReferences["fileName"] = "";
			$tempReferences["folder"] = "";
			$tempReferences["tempFile"];
			return $tempReferences;
		}

		public function saveFileExtension($file) {
			if (!$file || !$file["name"] || !$file["tmp_name"]) {
				$this->logger->logError(new \profilerobot\errors\FileErrorException());
			}	
			// in php 5.5< you could just chain element [2] at end of $explosion,
			// doing it in two steps makes it compatible with 5.3. 
			$explosion = explode(".", $file["name"]);
			$extension = end($explosion);
			$this->references["fileExtension"] = $extension;
		}

		// returns file extension without dot, example jpg or png
		public function getFileExtension () {
			return $this->references["fileExtension"];
		}

		// saves a reference of the file that was uploaded
		public function setTempfile ($file) {
			return $this->references["tempFile"] = $file;
		}

		// returns the reference to the file that was uploaded
		public function getTempfile () {
			return $this->references["tempFile"];
		}
		
		// runs all validations at once
		// true if all validation controls pass
		public function validate () {
			return (!$this->model->error($_FILES[$this->model->getFileAssociation()]) && $this->model->checksize($_FILES[$this->model->getFileAssociation()]) && $this->model->checkType($_FILES[$this->model->getFileAssociation()]));
		}

		// returns the name of the file, example filename.jpg
		public function getName () {
			return $this->references["fileName"];
		}
		
		// sets name to filename, example filename.jpg
		public function setName ($file) {
			if ($file === null || !isset($file) || $file["name"] === "" || $file["tmp_name"] === "") {
				$this->model->logError(new \profilerobot\errors\FileMoveException());
				return false;
			}
			// in php 5.5 you could just chain element [2] at end of $explosion,
			// doing it in two steps makes it compatible with 5.3. 
			$this->references["fileName"] = strip_tags($file["name"]);
		}
	
		// returns string such as uploads/phplLokkH/filename.jpg
		public function getImgSrc () {
			return $this->path() ."/" .$this->getName();
		}

		// return true if user has pushed upload button
		public function pictureIsUploaded () {
			$file = isset($_FILES[$this->model->getFileAssociation()]) ? $_FILES[$this->model->getFileAssociation()] : "file not set yet";
			return isset($_FILES[$this->model->getFileAssociation()]);
		}
		
		// sets path to a unique folder in root/uploads/sharelink__somerandomstring
		public function setPath () {
			$tmp = "uploads/".uniqid("sharelink_");
			$this->references["folder"] = $tmp;
		}

		// true if image is saved
		// saves name, 
		// creates temporary file to save to disk,
		// saves the extension that's used when creating resources
		public function saveImage () {
			$file = $this->file($this->model->getFileAssociation());
			$this->setName($file); // filename, example filename.jpg
			$this->setTempfile($file["tmp_name"]); // tempfile from uploaded img
			$this->setPath();
			$this->saveFileExtension($file); // .jpg, .jpeg or .png
		}
		
		// used by the image controller for passing things around between view and model
		public function path () {
			return $this->references["folder"];
		}

		// used for consistent naming of files and checking in globals
		public function file ($fileassociation) {
			return (isset($_FILES[$fileassociation]) ? $_FILES[$fileassociation] : null);
		}

		// reroutes the user to the created permalink
		public function reroute () {  
			return (is_string($this->path()) ? header('location: ' . $this->path()) : null);
		}
	}