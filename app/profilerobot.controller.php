<?php 
	
	namespace profilerobot\controller;

	require_once("profilerobot.model.php");
	require_once("profilerobot.view.php");
	require_once("image/image.controller.php");
	require_once("exceptions/buffer.exception.php");
	require_once("exceptions/fileerror.exception.php");

	class ProfileRobotController {
		// shared model with view
		private $model;
		private $view;

		// controller used for imagehandling
		private $imageController;
		
		// initiates the mvc
		public function __construct () {
			$this->imageController = new \image\controller\ImageController("ProfileRobot");
			$this->model = new \profilerobot\model\ProfileRobotModel();
			$this->view = new \profilerobot\view\ProfileRobotView($this->model);
			$this->view->setFileAssociation($this->imageController->fileAssociation()); 		
		}

		// page is rendered by echoing from view
		public function renderPage () {
			$this->view->render();
		}

		public function handleRequest () {
			session_start();
			// did user push upload button?
			if ($this->imageController->fileIsUploaded()) {
				// did user upload ok image?
				if ($this->imageController->validate()) {
					// was image saved correctly?
					try {
						$this->imageController->save();
						// save buffer for generating static page
						ob_start();	
						// array of copies from original
						$profilepics = $this->imageController->copy($this->model->getSocialMedia());
						// write generated view to buffer
						$this->view->renderGeneratedView($this->imageController->name(), $this->imageController->imgsrc(), $profilepics, $this->imageController->prefix());
						// contains generated view
						$static = ob_get_contents();
						ob_end_clean();
						// create an index.html in unique folder
						$this->imageController->staticPage($static);
						// reroute to unique folder
						$this->imageController->reroute();
						// add all copies to zipfile named as filename.zip
						$this->imageController->zip();
					} 
					catch (\profilerobot\errors\BufferException $e) {
						$this->model->logError(new $e);
					}
				}
				// validation failed, echo error msg.
				else {
						$this->view->alert($this->imageController->displayError());
						$this->model->logError(new \profilerobot\errors\FileErrorException());
						session_destroy();
				}
			}
			// }
		}
	}