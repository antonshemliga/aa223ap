<?php 
	namespace profilerobot\view;

	require_once('html/html.templates.php');
	require_once('exceptions/buffer.exception.php');
	require_once('logger/error.logger.php');

	class ProfileRobotView {
		// shared model with controller
		private $model;
		// html templates
		public $template;
		public $fileAssociation;
		// logging of errors
		private $logger;
		public function __construct (\profilerobot\model\ProfileRobotModel $model) {
			$this->model = $model;
			$this->template = new HTMLTemplate();
			$this->logger = new \errors\logger\Logger();
		}
		
		// position in $_FILES, passed in as filename when generating form 
		public function setFileAssociation ($fileAssociation) {
			$this->fileAssociation = $fileAssociation;
		}

		public function alert ($text) {
			echo $this->template->warning($text);
		}

		// generates bootstrap friendly layout
		// first page layout, echoes when visiting main page
		public function render () {
			$render = "";
			$render .= $this->template->header();
			$render .= $this->template->element("h1","Nice to meet you,<br> I'm Profile Robot.");
			$render .= $this->template->element("p","Choose your nicest looking self and I'll do all of the work.");
			$render .= $this->template->element("p","Start by choosing your image file.");
			$render .= $this->template->form($this->fileAssociation);
			$render .= $this->template->scripts();
			$render .= $this->template->closingTag("body");
			$render .= $this->template->element("footer","<p>Idea and web development by <a href='http://www.antonkandersson.se' title='Anton K. Andersson web developer'>Anton K. Andersson</a>.<br>Source code is up for grabs at <a href='https://github.com/antonkandersson/profilerobot/' title='Profile Robot by Anton K. Andersson on Github'>Github</a>.<br><a href='http://fezvrasta.github.io/bootstrap-material-design/'>Material Design for Bootstrap</a> by <a href='https://github.com/FezVrasta' title='Fez Vrazta on Github'>Fez Vrazta</a>.</p>");
			$render .= $this->template->closingTag("html");
			echo $render;
		}

		// generates bootstrap friendly layout
		// layout echoes when images have been created
		public function renderGeneratedView ($filename, $imgsrc, array $images, $prefix) {
			if (!$filename || !$imgsrc || !$images || !$prefix) {
				$this->logger->logError(new \profilerobot\errors\EchoHTMLException());
				return;
			}
			$render = "";
			$render .= $this->template->generatedHeader();
			$render .= $this->template->container();
			$render .= $this->template->row();
			$render .= $this->template->column();
			$render .= $this->template->sidebar($filename, $imgsrc, $prefix);
			$render .= $this->template->closingTag("div");
			$render .= $this->template->generatedImages($imgsrc, $prefix);
			$render .= $this->template->closingTag("div");
			$render .= $this->template->generatedScripts();
			$render .= $this->template->closingTag("body");
			$render .= $this->template->closingTag("html");
			echo $render;
		}
	} 