<?php 
	namespace profilerobot\model;
	require_once("./app/logger/error.logger.php");

	class ProfileRobotModel {
		private $logger;
		// array of social medias the app generates images for
		// each element should contain an array,
		// with it's corresponding image format specified in px as w * h
		private $socialmedia = array(
			array("name" => "Facebook", "dimensions" => array(340, 340)),
			array("name" => "GooglePlus", "dimensions" => array(280, 280)),
			array("name" => "Twitter", "dimensions" => array(224, 224)),
			array("name" => "LinkedIn", "dimensions" => array(360, 360)),
			array("name" => "Instagram", "dimensions" => array(500, 500)),
			array("name" => "Pinterest", "dimensions" => array(220, 220))
		);
				
		public function __construct () {
			$this->logger = new \errors\logger\Logger();
		}

		public function getSocialMedia () {
			return $this->socialmedia;
		}

		public function logError ($errormsg) {
			$this->logger->logError($errormsg);
		}
	}