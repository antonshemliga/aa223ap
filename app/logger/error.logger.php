<?php 
	namespace errors\logger;
	// Used for writing errors to disk. 
	// Inserts error msg into errorlog.txt.
	// Error msg is a string or Exception passed in.
	// 
	// Adds date and time for when the error was logged.
	// 
	// Example of message written, by passing in an Exception:
	// 
	// 2014-10-22-09:15:01pm
	// exception 'Exception' in /var/www/folder/project/app/profilerobot.controller.php:31
	// Stack trace:
	// #0 /var/www/folder/project/index.php(5): profilerobot\controller\ProfileRobotController->handleRequest()
	// #1 {main}
	// 
	class Logger {
		public function __construct () {

		}

		public function logError ($errormsg) {
			if ($errormsg) {
				date_default_timezone_set("Europe/Stockholm");
				$log = "errorlog.txt";
				$time = date("h:i:sa");
				$date = date("Y-m-d");
				$errorlog = "\n".$date."-".$time."\n".$errormsg."\n";
				file_put_contents($log, $errorlog, FILE_APPEND | LOCK_EX);
			}
		}
	}